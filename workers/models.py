from django.db import models
from django.contrib.auth.models import User

class Contractor(models.Model):
    first_name=models.CharField(max_length=30)
    last_name=models.CharField(max_length=30)
    age=models.IntegerField()
    email=models.EmailField()
    phone_number=models.CharField(max_length=15)
    gender = models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='M', max_length=1)    
    address=models.TextField()
    def __str__(self):
        return f"{self.first_name} {self.last_name}"
class Worker(models.Model):
    first_name=models.CharField(max_length=30)
    last_name=models.CharField(max_length=30)
    age=models.IntegerField()
    email=models.EmailField()
    phone_number=models.CharField(max_length=15)
    gender = models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='M', max_length=1)    
    address=models.TextField()
    work_type=models.CharField(max_length=50)
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    def __str__(self):
        return f"{self.first_name} {self.last_name}"
class Customer(models.Model):
    first_name=models.CharField(max_length=30)
    last_name=models.CharField(max_length=30)
    age=models.IntegerField()
    email=models.EmailField()
    phone_number=models.CharField(max_length=15)
    gender = models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='M', max_length=1)    
    address=models.TextField()
    def __str__(self):
        return f"{self.first_name} {self.last_name}"
class Chat(models.Model):
    chat=models.CharField(max_length=100)
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_chats')
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_chats')
class Contractor_request(models.Model):
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    details=models.TextField()
    rating=models.CharField(max_length=50)
    status = models.BooleanField(default=False)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
class Customer_complaint(models.Model):
    complaint=models.TextField()
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    reply=models.CharField(max_length=80)
class Feedback(models.Model):
    feedback=models.TextField()
    date = models.DateField(auto_now_add=True)
    name= models.ForeignKey(User, on_delete=models.CASCADE)
class Job(models.Model):
    title=models.CharField(max_length=20)
    details=models.TextField()
    location=models.CharField(max_length=50)
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    def __str__(self):
        return f"{self.title}"
class review(models.Model):
    review = models.TextField()
    date = models.DateField(auto_now_add=True)
    rating = models.IntegerField()  
    name= models.ForeignKey(User, on_delete=models.CASCADE)
class Job_details(models.Model):
    job=models.ForeignKey(Job, on_delete=models.CASCADE)
    worker=models.ForeignKey(Worker, on_delete=models.CASCADE)

class Worker_allocation(models.Model):
    date = models.DateField(auto_now_add=True)
    status = models.BooleanField(default=False)
    request = models.ForeignKey(Contractor_request, on_delete=models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
class Work_info_contr(models.Model):
    work_details=models.TextField()
    image=models.ImageField(upload_to='profile',default="")
    date = models.DateField(auto_now_add=True)
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
class Work_info_worker(models.Model):
    work_details=models.TextField()
    image=models.ImageField(upload_to='profile',default="")
    date = models.DateField(auto_now_add=True)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
class worker_request(models.Model):
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    details=models.TextField()
    rating=models.CharField(max_length=50)
    status = models.BooleanField(default=False)
    name= models.ForeignKey(User, on_delete=models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
class worker_complaint(models.Model):
    complaint=models.TextField()
    date = models.DateField(auto_now_add=True)
    time = models.TimeField(auto_now_add=True)
    reply=models.CharField(max_length=80)
    contractor = models.ForeignKey(Contractor, on_delete=models.CASCADE)
    name= models.ForeignKey(User, on_delete=models.CASCADE)

